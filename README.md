# ERROR pages generator
## Usage
* Install dependencies:

```
# pip install -r requirements
```
* Copy `config.example.py` to `config.py`
* Edit `config.py`
* Execute `gen.py`:

```
$ python gen.py
```
* Include configs into your Nginx config, example:

```
<...>
http {
    <...>
    server {
        <...>

        include <path to errPages.conf>;

        <...>
    }

    include <path to nginx.conf>;

    <...>
}
```
* Restart Nginx, on Arch:

```
# systemctl restart nginx.service
```
