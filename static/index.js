// Functions
startReloadTester = function () {
    setTimeout(function () {
        $.ajax({
            type: "HEAD",
            async: true,
            url: location.href,
            success: function(message, text, response) {
                if (response.status === 200) {
                    location.reload();
                } else {
                    startReloadTester();
                }
            },
            error: function(message, text, response) {
                startReloadTester();
            }
        });
    }, 2000)
}

// Init stuff
$.initialize('.nexus', function () {
    this.onmousedown = function (e) {
        var srcElement = e.srcElement;
        if (srcElement == undefined) {
            srcElement = e.target;
        }
        if (srcElement.tagName === 'IMG' || srcElement.tagName === 'I') {
            srcElement = srcElement.parentElement;
        }
        if ($(srcElement).is('select')) {
            $(srcElement).addClass('nexusEffect');
            setTimeout(function () {
                $(this).removeClass('nexusEffect');
            }.bind(srcElement), 750);
        } else {
            var x = (e.offsetX == undefined) ? e.layerX : e.offsetX;
            var y = (e.offsetY == undefined) ? e.layerY : e.offsetY;
            var effect = document.createElement('div');
            effect.className = 'effect';
            effect.style.top = (y + srcElement.scrollTop) + 'px';
            effect.style.left = (x + srcElement.scrollLeft) + 'px';
            srcElement.appendChild(effect);
            srcElement.style.overflow = 'hidden';
            setTimeout(function () {
                if ($(this).attr('onnexus') != undefined) {
                    eval($(this).attr('onnexus'));
                }
            }.bind(srcElement), 200);
            setTimeout(function () {
                this.removeChild(effect);
                this.style.overflow = null;
            }.bind(srcElement), 1500);
        }
    }
});

$.initialize('#backButton', function () {
    if (history.length < 2) {
        $(this).addClass('hidden');
    }
    this.onclick = function (e) {
        history.back();
    }
});

$.initialize('#reloadButton', function () {
    this.onclick = function (e) {
        location.reload(true);
    }
});
