import json

name = 'example.com'

templateGlobals = {'name': name,
                   'domain': 'error.example.com',
                   'homeUrl': 'https://example.com/',
                   'pathToFakeError': True,
                   'ssl': True,
                   'additionalNginxConfigs': ['ssl.conf'],
                   'debug': False,
                   'dongers': json.dumps({f'hug.{name}': '༼ つ ◕_◕ ༽つ',
                                          f'tableflip.{name}': '(╯°□°)╯︵ ┻━┻',
                                          f'tablefix.{name}': '┬─┬ノ( ◕◡◕ ノ)'})}
paths = {'builds': 'builds',
         'templates': 'templates',
         'static': 'static',
         'configs': 'configs'}
