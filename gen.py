#!/usr/bin/python3
import os
import jinja2
import config
from htmlmin.main import minify

os.chdir(os.path.dirname(os.path.realpath(__file__)))

config.path = os.path.dirname(os.path.realpath(__file__))

for path in ['builds', 'configs']:
    if os.path.exists(config.paths[path]):
        for root, dirs, files in os.walk(config.paths[path], topdown=False):
            for f in files:
                os.remove(os.path.join(root, f))
            for d in dirs:
                os.rmdir(os.path.join(root, d))
    else:
        os.mkdir(config.paths[path])

env = jinja2.Environment(loader=jinja2.FileSystemLoader(config.paths['templates']))

env.trim_blocks = True
env.lstrip_blocks = True

staticFileHashes = {}
for root, dirs, files in os.walk(config.paths['static'], topdown=False):
    for file in files:
        with open(os.path.join(config.paths['static'], file), 'r') as f:
            staticFileHashes[os.path.join(root, file)] = (str(hash(f.read()))[:3])

config.templateGlobals['join'] = os.path.join
config.templateGlobals['staticFileHashes'] = staticFileHashes
config.templateGlobals['supportedErrors'] = [template[:template.rfind('.')] for template in os.listdir(config.paths['templates']) if template[:1] != '_' and template.rfind('.conf') == -1]
config.templateGlobals['paths'] = {name: os.path.join(config.path, config.paths[name]) for name in config.paths}

for template in sorted(os.listdir(config.paths['templates'])):
    if template[:1] == '_':
        continue

    data = env.get_template(template).render(config.templateGlobals)

    if template.rfind('.conf') != -1:
        path = config.paths['configs']
        if data[-1:] != '\n':
            data += '\n'
    else:
        path = config.paths['builds']
        data = minify(data)

    with open(os.path.join(path, template), 'w') as f:
        f.write(data)

    if template.rfind('.html') != -1:
        print('rendered', template[:template.rfind('.html')])
    else:
        print('rendered', template)
